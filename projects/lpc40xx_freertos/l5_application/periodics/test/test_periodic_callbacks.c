#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockacceleration.h"
#include "Mockcan_bus.h"
#include "Mockcan_bus_initializer.h"
#include "Mockcan_bus_message_handler.h"
#include "Mockdrop_detection.h"

// Include the source we wish to test
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  acceleration__init_ExpectAndReturn(true);
#if SJTWO_RECEIVER
  initialize_drop_detector_LED_Expect();
#endif
  custom_can_initialize_ExpectAndReturn(can1, true);
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) {
  reset_can_bus_ExpectAndReturn(true);
  periodic_callbacks__1Hz(0);
}

void test__periodic_callbacks__10Hz(void) { periodic_callbacks__10Hz(0); }

void test__periodic_callbacks__100Hz(void) {
#if SJTWO_TRANSMITTER
  custom_can_tx_ExpectAndReturn(true);
#endif
#if SJTWO_RECEIVER
  custom_can_rx_ExpectAndReturn(true);
#endif
  periodic_callbacks__100Hz(0);
}