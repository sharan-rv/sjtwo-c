#include "periodic_callbacks.h"
#include "acceleration.h"
#include "can_bus_initializer.h"
#include "can_bus_message_handler.h"
#include "drop_detection.h"

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  custom_can_initialize(can1);
  acceleration__init();
#if SJTWO_RECEIVER
  initialize_drop_detector_LED();
#endif
}

void periodic_callbacks__1Hz(uint32_t callback_count) { reset_can_bus(); }

void periodic_callbacks__10Hz(uint32_t callback_count) {}

void periodic_callbacks__100Hz(uint32_t callback_count) {
#if SJTWO_TRANSMITTER
  custom_can_tx();
#endif
#if SJTWO_RECEIVER
  custom_can_rx();
#endif
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {}