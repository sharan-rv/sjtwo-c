#include "Mockcan_bus.h"
#include "can_bus_initializer.h"
#include "unity.h"

void test_custom_can_initialize_success(void) {
  const uint32_t test_baud_rate_kbps = 100;
  const can__num_e test_can_name = can1;

  can__init_ExpectAndReturn(can1, test_baud_rate_kbps, 1, 1, NULL, NULL, true);
  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_Expect(can1);
  TEST_ASSERT_TRUE(custom_can_initialize(test_can_name));
}

void test_can_init_fail(void) {
  const can__num_e test_can_name = can1;

  can__init_ExpectAnyArgsAndReturn(false);
  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_Expect(can1);
  TEST_ASSERT_FALSE(custom_can_initialize(test_can_name));
}

void test_reset_can_bus_success(void) {
  const can__num_e test_can_name = can1;

  can__is_bus_off_ExpectAndReturn(test_can_name, true);
  can__reset_bus_Expect(test_can_name);

  TEST_ASSERT_TRUE(reset_can_bus());
}

void test_reset_can_bus_bus_off(void) {
  const can__num_e test_can_name = can1;

  can__is_bus_off_ExpectAndReturn(test_can_name, false);
  can__reset_bus_Ignore();

  TEST_ASSERT_FALSE(reset_can_bus());
}
