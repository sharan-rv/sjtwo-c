#pragma once

#include "can_bus.h"

bool custom_can_initialize(can__num_e can_name);
bool reset_can_bus(void);