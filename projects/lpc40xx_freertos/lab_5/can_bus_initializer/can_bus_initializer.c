#include "can_bus_initializer.h"
#include "stddef.h"

bool custom_can_initialize(can__num_e can_name) {
  bool return_value = false;
  const uint32_t can_bus_baud_rate_kbps = 100;
  const uint16_t tx_queue_size = 1;
  const uint16_t rx_queue_size = 1;

  return_value = can__init(can_name, can_bus_baud_rate_kbps, rx_queue_size, tx_queue_size, NULL, NULL);
  can__bypass_filter_accept_all_msgs();
  can__reset_bus(can_name);

  return return_value;
}

bool reset_can_bus(void) {
  bool return_value = false;

  if (can__is_bus_off(can1)) {
    can__reset_bus(can1);
    return_value = true;
  }

  return return_value;
}