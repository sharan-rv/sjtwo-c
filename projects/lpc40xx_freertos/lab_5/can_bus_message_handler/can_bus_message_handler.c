#include "can_bus_message_handler.h"
#include "can_bus.h"
#include "drop_detection.h"

static const uint8_t board_dropped_message = 0xAA;
static const uint8_t board_not_dropped_message = 0xBA;
static const uint32_t message_id_drop_detection = 0x123;

static bool detected_drop_from_sj2_transmitter(can__msg_t *message_frame);
static bool no_drop_detected_from_sj2_transmitter(can__msg_t *message_frame);
static void initalize_tx_frame_parameters(can__msg_t *message_frame);

bool custom_can_tx(void) {
  bool return_value = false;
  can__msg_t message_tx_frame = {0};

  initalize_tx_frame_parameters(&message_tx_frame);

  if (detect_drop_event(acceleration__get_data())) {
    message_tx_frame.data.bytes[0] = board_dropped_message;
  } else {
    message_tx_frame.data.bytes[0] = board_not_dropped_message;
  }

  return_value = can__tx(can1, &message_tx_frame, 1);

  return return_value;
}

bool custom_can_rx(void) {
  bool return_value = false;
  can__msg_t message_rx_frame = {0};

  if (can__rx(can1, &message_rx_frame, 1)) {
    return_value = true;
  }

  if (return_value) {
    if (detected_drop_from_sj2_transmitter(&message_rx_frame)) {
      gpio__reset(get_drop_detector_LED());
    } else if (no_drop_detected_from_sj2_transmitter(&message_rx_frame)) {
      gpio__set(get_drop_detector_LED());
    }
  }

  return return_value;
}

static bool detected_drop_from_sj2_transmitter(can__msg_t *message_frame) {
  return ((board_dropped_message == message_frame->data.bytes[0]) &&
          (message_id_drop_detection == message_frame->msg_id));
}

static bool no_drop_detected_from_sj2_transmitter(can__msg_t *message_frame) {
  return (board_not_dropped_message == message_frame->data.bytes[0]) &&
         (message_id_drop_detection == message_frame->msg_id);
}

static void initalize_tx_frame_parameters(can__msg_t *message_frame) {
  message_frame->msg_id = message_id_drop_detection;
  message_frame->frame_fields.is_29bit = 0;
  message_frame->frame_fields.data_len = 1;
}