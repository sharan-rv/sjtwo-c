#pragma once

#include "gpio.h"
#include <stdbool.h>

bool custom_can_tx(void);
bool custom_can_rx(void);