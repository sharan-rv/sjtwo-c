#include "drop_detection.h"
#include "board_io.h"

static gpio_s drop_detector_LED;

int32_t calculate_magnitude_vector(acceleration__axis_data_s *input_acceleration) {
  int32_t magnitude_acceleration =
      ((int32_t)input_acceleration->x * input_acceleration->x + (int32_t)input_acceleration->y * input_acceleration->y +
       (int32_t)input_acceleration->z * input_acceleration->z) /
      3;

  return magnitude_acceleration;
}

bool detect_drop_event(acceleration__axis_data_s input_acceleration) {
  bool return_value = false;
  const int32_t drop_threshold = 1500000;

  int32_t magnitude_vector = calculate_magnitude_vector(&input_acceleration);

  if (magnitude_vector > drop_threshold) {
    return_value = true;
  }

  return return_value;
}

void initialize_drop_detector_LED(void) { drop_detector_LED = board_io__get_led3(); }
gpio_s get_drop_detector_LED(void) { return drop_detector_LED; }
