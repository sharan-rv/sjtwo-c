#pragma once

#include "acceleration.h"
#include "gpio.h"
#include <stdbool.h>

bool detect_drop_event(acceleration__axis_data_s input_acceleration);
void initialize_drop_detector_LED(void);
gpio_s get_drop_detector_LED(void);